## WHAT

Sequence Diagrams for actors is an interaction diagram that details communications between different actors to perform some objectives. 

## **WHY**

To analyze tasks in a sequence for how actors interact with the system/other actors to fulfill their needs/goals.

## **HOW**

1. Create the list of all the actors with their primary goals
2. Define how an actor interacts with other actors to fulfill his tasks/goals.
3. Arrange all the tasks in a sequence involving all actors.
4. Tools to create sequence diagrams are: [Mermaid](https://mermaid-js.github.io/), [Sequencediagram.org](https://sequencediagram.org/)

## [Example:](https://stackoverflow.com/questions/23169328/two-actors-invoking-same-functionality-in-sequence-diagram-how-to-represent##:~:text=Here)

**Sequence Diagram for BestofBharat**

```mermaid
sequenceDiagram
    autonumber

    participant CRM System as CRM System;
    Note over CRM System: Machine actor 'CRM system' responsible for sending triggers on any activity

    participant Lead as Lead;
    Note over Lead: John, come in through social media like WhatsApp with live chat.

   participant Owner as Owner;
    Note over Owner: Owner Ricky who monitors and handles the admin panel of CRM.

   participant Bot as Bot;
    Note over Bot: Hybrid chat Bot to send "Updates" to other actors on Triggers

    participant sales rep as sales rep;
    Note over sales rep: Owner's Colleagues like Charan who handles customer support and leads.

    participant Manager as Manager;
    Note over Manager: Owner's colleague like Khushbu who takes update from sales rep and handle escalations/delays.

    CRM System->>Bot: TRIGGGERS arrival of new actor
    Note over CRM System,Bot: Triggered message: "Hi , new message received by visitor "John" through WhatsApp"

    Owner->>CRM System: CREATES/ASSIGNS task pipeline
    Note over Bot,CRM System: Event "Deal assigned" Triggered.

    CRM System->>Bot: TRIGGGERS Deal assigned
    Note over Bot, sales rep : Update to assigned member:<br> "Hi <Charan>, Please pick the leads assigned to you."

    sales rep->>Lead: COMMUNICATE to Visitor to initiate conversation
    Note over sales rep,Lead: "Hi there, I see you checked our Pricing page. <br> I can arrange custom pricing for you?'"

    Lead->>sales rep: RESPONSE to sales rep
    Note over Lead,sales rep: " Hi, Sure, Can we connect through a call where I can explain my needs?" <br> TRIGGERED Meeting scheduled event.

    Lead->>CRM System: PURCHASE order
    Note over CRM System,Bot: "Order purchased" trigger activated. 


    Bot->>sales rep: UPDATE order purchased
    Note over Bot, sales rep: " John placed an order for his wall decoration" <br> Trigger "Payment received "activated.

    Lead-->>sales rep: ASK FAQ/Support
    Note over Lead,sales rep: "How can I adjust my pricing with more purchases?" <br> Status: Query resolved

    sales rep->>Lead: COMMUNICATE to Customer for deal closing.
    Note over sales rep,Lead: "Hi there, Thanks for the purchase you made. <br> Hope you had great experience with us."

    sales rep->>Manager: UPDATE the lead status as completed
    Note over sales rep,Manager: Update: Lead of "John" has been "completed" successfully. <br> Kick-off amount received. 

    Manager->> Owner: MONITORS the lead and paid invoices
    Note over Manager, Owner: Update: 30 Leads in WON and 5 leads are On-hold today, $3000
```

**Example 2 with fewer tasks and actors:**

 ```mermaid
sequenceDiagram
    autonumber

   participant Owner as Owner;
    Note over Owner: Owner Ricky monitors and handles the admin panel of CRM.

    participant Lead as Lead;
    Note over Lead: Visitor John, come in through social media like whatsapp with live chat.

    participant sales rep as sales rep;
    Note over sales rep: Owner's Colleagues like Charan who handles customer support and leads.

    participant Manager as Manager;
    Note over Manager: Owner's colleague like Khushbu who takes update from sales rep and handle escalations/delays.

    Owner->>sales rep: CREATES/ASSIGNS task pipeline
    Note over Owner,sales rep: Charan gets notification of deal assigned to him

    sales rep->>Lead: COMMUNICATE to Visitor to initiate conversation
    Note over sales rep,Lead: "Hi there, I see you checked our Pricing page. <br> I can arrange custom pricing for you?'"

    Lead->>sales rep: ASK FAQ/Support
    Note over Lead,sales rep: "How can I adjust my pricing with more purchases" <br> Query resolved

Lead->>sales rep: PURCHASE order
    Note over Lead,sales rep: Paid Invoice shared by the lead

    sales rep->>Manager: UPDATE the lead status as completed
    Note over sales rep,Manager: Update: Lead of "John" has been "completed" successfully. <br> Kick off amount received. 

    Manager->> Owner: MONITORS the lead and paid invoices
    Note over Manager, Owner: Update: 30 Leads in WON and 5 leads are On-hold today, $3000
```
